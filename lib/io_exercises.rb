# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.


def guessing_game
  number = 1 + rand(100)
  num_guesses = 0

  until false
    puts "Guess a number between 1 and 100!"
    guess = Integer(gets.chomp)
    num_guesses += 1

    if guess > number
      puts "#{guess} is too high. Try again!"
    elsif guess < number
      puts "#{guess} is too low. Try again!"
    elsif guess == number
      puts "#{guess} is the correct number! Nice work!"
      break
    else
      raise error
    end
  end
  puts "You have guessed #{num_guesses} times."
end

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def shuffle_file(filename)
  base = File.basename(filename, ".*")
  extension = File.extname(filename)
  File.open("#{base}-shuffled#{extension}", 'w') do |f|
    File.readlines(filename).shuffle.each do |line|
      f.puts line.chomp
    end
  end

end
